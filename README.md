### INSTALATION
## DEVELOPMENT PREREQUISITES
1. Docker
2. Node.js v10.16.0

## SERVER
1. `cp ormconfig.json.example ormconfig.json`
2. run mysql from docker
```
docker run --net=host --name flycost -e MYSQL_ROOT_PASSWORD=flycost -e MYSQL_DATABASE=flycost -e MYSQL_USER=flycost -e MYSQL_PASSWORD=flycost -d mysql:5.7.26
```
3. `npm i && npm start`
