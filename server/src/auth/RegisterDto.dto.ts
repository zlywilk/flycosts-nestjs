import { IsEmail, IsNotEmpty, MinLength } from "class-validator";

export class RegisterDto{
    id:string;
    @IsEmail()
    @IsNotEmpty()
    email:string;
    @IsNotEmpty()
    @MinLength(6, {message: "Password is too short. Minimal length is $value characters" })
    password:string;
    passwordHash:string;
}