import { Controller, Post, HttpStatus, HttpCode, Get, Response, Body, UsePipes, ValidationPipe } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserService } from '../user/user.service';
import { RegisterDto } from './RegisterDto.dto';
import { LoginDto } from './LoginDto.dto';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService) {}

  // TODO: bad typing - create type named like LoginDto
  @Post('login')
  async loginUser(@Response() res: any, @Body() body: LoginDto) {

    const user = await this.userService.getUserByEmail(body.email);
    
    if (user) {
      if (await this.userService.compareHash(body.password, user.passwordHash)) {
        return res.status(HttpStatus.OK).json(await this.authService.createToken(user.id, user.email));
      }
    }

    return res.status(HttpStatus.FORBIDDEN).json({ message: 'Email or password wrong!' });
  }

  // TODO: create typing like RegisterDto
  @Post('register')
  async registerUser(@Response() res: any, @Body() body: RegisterDto) {


    let emailuser = await this.userService.getUserByEmail(body.email);

    if (emailuser) {
      return res.status(HttpStatus.FORBIDDEN).json({ message: 'Email exists' });
    } else {
      emailuser = await this.userService.createUser(body);
      if (emailuser) {
        emailuser.passwordHash = undefined;
      }
    }

    return res.status(HttpStatus.OK).json({email:emailuser.email, id:emailuser.id});
  }
}