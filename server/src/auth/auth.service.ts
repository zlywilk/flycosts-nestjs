import * as jwt from 'jsonwebtoken';
import { Injectable } from '@nestjs/common';
import { UserService } from '../user/user.service';

@Injectable()
export class AuthService {
  constructor(private readonly userService: UserService) { }

  async createToken(id: string, email: string) {
    const expiresIn = 60 * 60;
    const secretOrKey = 'secret';
    const emailUser = { email };
    const token = jwt.sign(emailUser, secretOrKey, { expiresIn });

    return { expires_in: expiresIn, token, email  };
  }

  async validateUser(signedUser): Promise<boolean> {
    if (signedUser&&signedUser.email) {
      return await !!(this.userService.getUserByEmail(signedUser.email));
    }

    return false;
  }
}