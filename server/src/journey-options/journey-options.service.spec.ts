import { Test, TestingModule } from '@nestjs/testing';
import { JourneyOptionsService } from './journey-options.service';

describe('JourneyOptionsService', () => {
  let service: JourneyOptionsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [JourneyOptionsService],
    }).compile();

    service = module.get<JourneyOptionsService>(JourneyOptionsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
