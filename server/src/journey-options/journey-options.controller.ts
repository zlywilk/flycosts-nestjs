import { Controller, Post, Body, Get, Put, Delete, Param, UsePipes} from '@nestjs/common';
import {  journeyOptionsService } from './journey-options.service';
import { journeyOption } from './journey-option.entity';
import { journeyOptionsDTO } from './journey-options.dto';

@Controller('api/journey-options')
export class JourneyOptionsController {

    constructor(private service: journeyOptionsService) { }

    @Get()
    async getOptions() {
        return this.service.getOptions();
    }
 

    @Post()
    
   async createOption(@Body() journeyOptionData: journeyOptionsDTO) {
        return await this.service.createOption(journeyOptionData);
    }

    @Put(':id')
    async updateOption(@Param('id') id:string, @Body() journeyOptionData: Partial<journeyOptionsDTO>) {
        return this.service.updateOption(id, journeyOptionData);
    }

    @Delete(':id')
    async deleteOption(@Param('id') id:string) {
        return this.service.deleteOption(id);
    }
}