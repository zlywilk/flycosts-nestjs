import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class journeyOption {
  @PrimaryGeneratedColumn()
  id: string;

  @Column('text')
  carrier: string;

  @Column('text')
  origin: string;

  @Column('text')
  destination: string;

  @Column('text')
  flyIn: string;

  @Column('text')
  flyOut: string;
}
