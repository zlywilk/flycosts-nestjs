import { IsString } from 'class-validator';

export class journeyOptionsDTO{
    @IsString()
    carrier:string;
    @IsString()
    origin:string;
    @IsString()
    destination:string;
    @IsString()
    flyIn:string;
    @IsString()
    flyOut:string;

}