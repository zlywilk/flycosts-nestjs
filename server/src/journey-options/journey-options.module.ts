import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { journeyOptionsService } from './journey-options.service';
import { JourneyOptionsController } from './journey-options.controller';
import { journeyOption } from './journey-option.entity';

@Module({
  imports:[TypeOrmModule.forFeature([journeyOption])],
  providers: [journeyOptionsService],
  controllers: [JourneyOptionsController]
})
export class JourneyOptionsModule {}
