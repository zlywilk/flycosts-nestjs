import { Test, TestingModule } from '@nestjs/testing';
import { JourneyOptionsController } from './journey-options.controller';

describe('JourneyOptions Controller', () => {
  let controller: JourneyOptionsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [JourneyOptionsController],
    }).compile();

    controller = module.get<JourneyOptionsController>(JourneyOptionsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
