import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { journeyOption } from './journey-option.entity';
import { journeyOptionsDTO } from './journey-options.dto';

@Injectable()
export class journeyOptionsService {

    constructor(@InjectRepository(journeyOption) 
    private journeyOptionRepository: Repository<journeyOption>) { }

    async createOption(journeyOption:journeyOptionsDTO) {
        const journey = await this.journeyOptionRepository.create(journeyOption);
        await this.journeyOptionRepository.save(journey);
        return journey;
    }

    async getOptions(): Promise<journeyOption[]> {
        return await this.journeyOptionRepository.find();
    }


    async updateOption(id:string, journeyOption:Partial<journeyOptionsDTO>) {
        let journey = await this.journeyOptionRepository.findOne({where:{id}});
        if(!journey){
            throw new HttpException('not found', HttpStatus.NOT_FOUND);
        }
        await this.journeyOptionRepository.update({id}, journeyOption);
        journey = await this.journeyOptionRepository.findOne({where:{id}});
        return await journey;
    }

    async deleteOption(id) {
        const journey = await this.journeyOptionRepository.findOne({where:{id}});
        if(!journey){
            throw new HttpException('not found', HttpStatus.NOT_FOUND);
        }
        await this.journeyOptionRepository.delete({id});
        return journey;

    }
}