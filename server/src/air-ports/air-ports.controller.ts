import { Controller, Get } from '@nestjs/common';
import { AirPortsService } from "./air-ports.service";

@Controller('/api/air-ports')
export class AirPortsController {
    constructor(private service:AirPortsService) {}

    @Get()
    async getairPorts(){
        return await this.service.getAirPorts();
    }
}