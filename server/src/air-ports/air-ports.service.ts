import { Injectable } from '@nestjs/common';
import {airports} from './air-ports';
import {airPorts} from './air-ports.entity';
import { Observable, of } from 'rxjs';

@Injectable()
export class AirPortsService {
    public getAirPorts():Observable<airPorts[]> {
        return of(airports);
    }
}
