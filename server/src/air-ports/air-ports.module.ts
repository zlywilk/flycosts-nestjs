import { Module } from '@nestjs/common';
import { AirPortsService } from './air-ports.service';
import { AirPortsController } from "./air-ports.controller";


@Module({
  providers: [AirPortsService],
  controllers: [AirPortsController]
})
export class AirPortsModule {}
