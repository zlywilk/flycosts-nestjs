import { Test, TestingModule } from '@nestjs/testing';
import { AirPortsService } from './air-ports.service';

describe('AirPortsService', () => {
  let service: AirPortsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AirPortsService],
    }).compile();

    service = module.get<AirPortsService>(AirPortsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
