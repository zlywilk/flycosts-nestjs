import { Test, TestingModule } from '@nestjs/testing';
import { AirPortsController } from './air-ports.controller';

describe('AirPorts Controller', () => {
  let controller: AirPortsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AirPortsController],
    }).compile();

    controller = module.get<AirPortsController>(AirPortsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
