import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { JourneyOptionsModule } from './journey-options/journey-options.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from './user/user.module';
import { AuthService } from './auth/auth.service';
import { AuthController } from './auth/auth.controller';
import { AuthModule } from './auth/auth.module';
import { FlycostsModule } from './flycosts/flycosts.module';
import { HotelPriceModule } from './hotel-price/hotel-price.module';
import { AirPortsModule } from './air-ports/air-ports.module';
import { APP_FILTER} from '@nestjs/core';
import { HttpErrorFilter } from './shared/http-error.filter';

@Module({
  imports: [
    TypeOrmModule.forRoot(),
    JourneyOptionsModule,
    UserModule,
    AuthModule,
    FlycostsModule,
    HotelPriceModule,
    AirPortsModule],
  controllers: [AppController, AuthController],
  providers: [AppService,{
    provide:APP_FILTER, 
    useClass:HttpErrorFilter},
     AuthService],
})
export class AppModule {}
