import { Injectable, HttpException, HttpStatus} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, DeleteResult } from 'typeorm';
import { HotelPrice } from "./hotel-price.entity";
import { hotelPriceDTO } from './hotel-price.dto';

@Injectable()
export class HotelPriceService {
    constructor(@InjectRepository(HotelPrice)
    private HotelPriceRepository:Repository<HotelPrice>){}

    async createHotelPrice(HotelPrice: hotelPriceDTO){
        const hotel = await this.HotelPriceRepository.create(HotelPrice);
         await this.HotelPriceRepository.save(hotel);
        return hotel;
    }
    async getHotelPrice():Promise<HotelPrice[]> {
        return await this.HotelPriceRepository.find();
    }
    async deleteHotelPrice(id){
        const hotel = await this.HotelPriceRepository.findOne({where:{id}});
        if(!hotel){
            throw new HttpException('not found', HttpStatus.NOT_FOUND);
        }
         await this.HotelPriceRepository.delete({id});
         return hotel;
    }
}
