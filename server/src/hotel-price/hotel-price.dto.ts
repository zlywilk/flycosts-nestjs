import { IsNumber, IsDate } from 'class-validator';

export class hotelPriceDTO{
    @IsDate()
    flyIn:Date;
    @IsDate()
    flyOut:Date;
    @IsNumber()
    hotelPrice:number;

}