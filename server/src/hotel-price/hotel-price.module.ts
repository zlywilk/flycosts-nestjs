import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HotelPriceService } from './hotel-price.service';
import {  HotelPriceController} from "./hotel-price.controller";
import {  HotelPrice} from "./hotel-price.entity";

@Module({
  imports:[TypeOrmModule.forFeature([HotelPrice])],
  providers: [HotelPriceService],
  controllers:[HotelPriceController]

})
export class HotelPriceModule {}
