import { Test, TestingModule } from '@nestjs/testing';
import { HotelPriceService } from './hotel-price.service';

describe('HotelPriceService', () => {
  let service: HotelPriceService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HotelPriceService],
    }).compile();

    service = module.get<HotelPriceService>(HotelPriceService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
