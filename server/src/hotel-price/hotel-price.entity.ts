import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class HotelPrice {
    @PrimaryGeneratedColumn()
    id: string;
  
    @Column({type: "timestamp"})
    timestamp: Date;
  
  
    @Column()
    flyIn: Date;
  
    @Column()
    flyOut: Date;
    
    @Column()
    hotelPrice: number;
  
}
