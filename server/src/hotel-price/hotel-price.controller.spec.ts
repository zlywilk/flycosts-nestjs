import { Test, TestingModule } from '@nestjs/testing';
import { HotelPriceController } from './hotel-price.controller';

describe('HotelPrice Controller', () => {
  let controller: HotelPriceController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [HotelPriceController],
    }).compile();

    controller = module.get<HotelPriceController>(HotelPriceController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
