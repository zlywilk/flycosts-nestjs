import {
  Controller,
  Post,
  Body,
  Get,
  Put,
  Delete,
  Param,
  UsePipes,
} from '@nestjs/common';
import { HotelPriceService } from './hotel-price.service';
import { hotelPriceDTO } from './hotel-price.dto';

@Controller('api/hotel-price')
export class HotelPriceController {
  constructor(private service: HotelPriceService) {}

  @Get()
  async getHotelPrice() {
    return await this.service.getHotelPrice();
  }

  @Post()
  async createHotelPrice(@Body() HotelPriceData: hotelPriceDTO){
    return await this.service.createHotelPrice(HotelPriceData);
  }
  @Delete(':id')
  async deleteHotelPrice(@Param('id') id){
    return await this.service.deleteHotelPrice(id);
  }
}
