import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class flycosts {
  @PrimaryGeneratedColumn()
  id: string;

  @Column({type: "timestamp"})
  timestamp: Date;

  @Column({length: 20})
  carrier: string;

  @Column({length: 50})
  city: string;

  @Column()
  flyIn: Date;

  @Column()
  flyOut: Date;
  
  @Column()
  ticketPrice: number;
}
