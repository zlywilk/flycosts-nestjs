import { Injectable, Provider, HttpException, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, UpdateResult, DeleteResult } from 'typeorm';
import { flycosts } from "./flycosts.entity";
import { flycostsDTO } from './flycossts.dto';

@Injectable()
export class FlycostsService {
    constructor(@InjectRepository(flycosts)
    private flycostsRepository:Repository<flycosts>){}

    async createFlycosts(flycosts: flycostsDTO){
        const fly = await this.flycostsRepository.create(flycosts);
        await this.flycostsRepository.save(fly);
        return fly;
    }
    async getFlycosts():Promise<flycosts[]> {
        return await this.flycostsRepository.find();
    }
    async deleteFlycosts(id:string){
        const fly = await this.flycostsRepository.findOne({where:{id}});
        if(!fly){
            throw new HttpException('not found', HttpStatus.NOT_FOUND);
        }
        await this.flycostsRepository.delete({id});
        return fly;
    }
}
