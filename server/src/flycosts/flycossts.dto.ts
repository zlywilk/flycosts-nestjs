import { IsString, IsDate, IsNumber } from 'class-validator';

export class flycostsDTO{
    @IsString()
    carrier:string;
    @IsString()
    city:string;
    @IsDate()
    flyIn:Date;
    @IsDate()
    flyOut:Date;
    @IsNumber()
    ticketPrice: number;
}