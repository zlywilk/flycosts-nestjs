import {
  Controller,
  Post,
  Body,
  Get,
  Delete,
  Param,
} from '@nestjs/common';
import { FlycostsService } from './flycosts.service';
import { flycosts } from './flycosts.entity';
import { flycostsDTO } from './flycossts.dto';

@Controller('api/flycosts')
export class FlycostsController {
  constructor(private service: FlycostsService) {}

  @Get()
  async getFlycosts() {
    return await this.service.getFlycosts();
  }

  @Post()
  async createFlycosts(@Body() flycostsData: flycostsDTO) {
    return await this.service.createFlycosts(flycostsData);
  }
  @Delete(':id')
  async deleteFlycosts(@Param('id') id) {
    return await this.service.deleteFlycosts(id);
  }
}
