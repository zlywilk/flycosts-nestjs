import { Test, TestingModule } from '@nestjs/testing';
import { FlycostsController } from './flycosts.controller';

describe('Flycosts Controller', () => {
  let controller: FlycostsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FlycostsController],
    }).compile();

    controller = module.get<FlycostsController>(FlycostsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
