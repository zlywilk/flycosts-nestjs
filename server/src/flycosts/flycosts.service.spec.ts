import { Test, TestingModule } from '@nestjs/testing';
import { FlycostsService } from './flycosts.service';

describe('FlycostsService', () => {
  let service: FlycostsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FlycostsService],
    }).compile();

    service = module.get<FlycostsService>(FlycostsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
