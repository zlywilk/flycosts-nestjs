import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FlycostsService } from './flycosts.service';
import { FlycostsController  } from "./flycosts.controller";
import { flycosts } from "./flycosts.entity";

@Module({
  imports:[TypeOrmModule.forFeature([flycosts])],
  providers: [FlycostsService],
  controllers:[FlycostsController]
})
export class FlycostsModule {}
