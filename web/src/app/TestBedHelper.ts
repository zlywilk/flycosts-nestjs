import { TestBed } from '@angular/core/testing';

import { RouterTestingModule } from '@angular/router/testing';

import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { MatButtonModule, MatToolbarModule, MatSidenavModule, MatIconModule, MatListModule, MatInputModule, MatSelectModule, MatCardModule, MatAutocompleteModule, MatGridListModule, MatMenuModule } from '@angular/material';

import { FlexLayoutModule } from '@angular/flex-layout';

import { AppComponent } from './app.component';
import { ReactiveFormsModule } from '@angular/forms';

import { AuthService } from './services/auth.service';
import { MaterialDashboardComponent } from './components/material-dashboard/material-dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { RegiesterComponent } from './components/regiester/regiester.component';
import { ForgotpasswordComponent } from './components/forgotpassword/forgotpassword.component';
import { AuthServiceMock } from './services/auth.service.mock';

export class TestBedHelper {
  static createTestBed() {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        NoopAnimationsModule,
        HttpClientTestingModule,
        MatButtonModule,
        MatToolbarModule,
        MatSidenavModule,
        MatIconModule,
        MatListModule,
        MatInputModule,
        MatSelectModule,
        MatCardModule,
        MatAutocompleteModule,
        MatGridListModule,
        MatMenuModule,
        FlexLayoutModule,
        ReactiveFormsModule
      ],
      declarations: [
        AppComponent,
        MaterialDashboardComponent,
        LoginComponent,
        RegiesterComponent,
        ForgotpasswordComponent
      ],
      providers: [{
        provide: AuthService, useClass: AuthServiceMock
      }]
    }).compileComponents()
  }
}
