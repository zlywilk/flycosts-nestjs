export interface RegisterDTO {
  email: string;
  password: string;
  termsandconditions: boolean;
}
