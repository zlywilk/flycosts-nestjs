export interface LoginResponseDTO {
    expires_in: number;
    token: string;
    email:string;
  }