export interface RegisterResponseDTO {
  id: string;
  email: string;
}
