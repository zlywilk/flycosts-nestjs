import { TestBed, getTestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { TestBedHelper } from '../TestBedHelper';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { RegisterDTO } from '../models/register.dto';
import { LoginDTO } from '../models/login.dto';

describe('AuthService', () => {
  let injector: TestBed;
  let authService: AuthService;
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [

        HttpClientTestingModule,

      ],
      providers: [AuthService]
    }).compileComponents();
    injector = getTestBed();
    authService = injector.get(AuthService);
    httpMock = injector.get(HttpTestingController);
  });

  it('should be created', () => {
    const service: AuthService = TestBed.get(AuthService);
    expect(service).toBeTruthy();
  });

  it('should send post using httpClient with user', () => {
    const mockUser: RegisterDTO = {
      email: 'test@test.com',
      password: 'testPassword',
      termsandconditions: true
    };
    authService.register(mockUser)
      .subscribe(data => {
        expect(data.email).toBeDefined();
        expect(data.id);
      });
    const req = httpMock.expectOne('auth/register');

    expect(req.request.method).toEqual('POST');

    req.flush({
      email: 'thecyberd3m0n2@gmail.com',
      id: '264c3f4b-f098-4a01-85b5-1bedbfc85f8f'
    });
  });
  it('should send post using httpClient with user', () => {
    const mockUser: LoginDTO = {
      email: 'test@test.com',
      password: 'testPassword'
    };
    authService.login(mockUser)
      .subscribe(data => {
        expect(data).toBeDefined();

      });
    const req = httpMock.expectOne('auth/login');

    expect(req.request.method).toEqual('POST');

    req.flush({
      expires_in: 3600,
      token: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im15QGMuZGUiLCJpYXQiOjE1NjE4MDEyNDUsImV4cCI6MTU2MTgwNDg0NX0.XiiHvlp3B0RDAeWLUWsIwqDohijwM1nyc2ZzwUHAv94",
      email: "my@c.de"
  });
  });
});
