import { Injectable } from "@angular/core";
import { RegisterDTO } from "../models/register.dto";
import { Observable } from "rxjs";
import { HttpClient } from "@angular/common/http";
import { RegisterResponseDTO } from "../models/registerResposne.dto";
import { LoginDTO } from "../models/login.dto";
import { LoginResponseDTO } from "../models/loginResponse.dto";
import { tap, map } from "rxjs/operators";
import { JsonPipe } from "@angular/common";
import { Local } from "protractor/built/driverProviders";

const STORAGE_NAME = "AUTH";

@Injectable()
export class AuthService {
  constructor(private http: HttpClient) {}

  register(registerUserDTO: RegisterDTO): Observable<RegisterResponseDTO> {
    return this.http.post<RegisterResponseDTO>(
      "auth/register",
      registerUserDTO
    );
  }
  login(loginDTO: LoginDTO): Observable<string> {
    return this.http.post<LoginResponseDTO>("auth/login", loginDTO).pipe(
      tap((payload: LoginResponseDTO) => {
        //save user token
  
      }),
      map((payload: LoginResponseDTO) => JSON.stringify({'expires_in':payload.expires_in, 'token':payload.token, 'email':payload.email}))
    )
  }
  getStorage(): LoginResponseDTO {
    return JSON.parse(localStorage.getItem(STORAGE_NAME));
  }
  setItem(item) {
    localStorage.setItem(STORAGE_NAME, item);
  }
}