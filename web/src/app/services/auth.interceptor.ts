import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { AuthService } from './auth.service';
import { Observable } from 'rxjs';

export class AuthInterceptor implements HttpInterceptor {
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (this.authService.getStorage()) {
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${this.authService.getStorage().token}`
        }
      });
    }
    return next.handle(req);
  }
  constructor(public authService: AuthService) {}
}
