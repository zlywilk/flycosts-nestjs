import { LayoutModule } from '@angular/cdk/layout';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatButtonModule,
  MatCardModule,
  MatGridListModule,
  MatIconModule,
  MatMenuModule,
} from '@angular/material';

import { MaterialDashboardComponent } from './material-dashboard.component';
import { TestBedHelper } from 'src/app/TestBedHelper';

describe('MaterialDashboardComponent', () => {
  let component: MaterialDashboardComponent;
  let fixture: ComponentFixture<MaterialDashboardComponent>;

  beforeEach(async(() => {
    TestBedHelper.createTestBed();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MaterialDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
