import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { LoginResponseDTO } from 'src/app/models/loginResponse.dto';
import { LoginDTO } from 'src/app/models/login.dto';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  constructor(public _AuthService: AuthService) {}
  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl(),
      password: new FormControl()
    });
  }
  onSubmit(){
    return this._AuthService.login(this.loginForm.value).subscribe(data=>this._AuthService.setItem(data));;
  }
}
