import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegiesterComponent } from './regiester.component';
import { TestBedHelper } from 'src/app/TestBedHelper';

describe('RegiesterComponent', () => {
  let component: RegiesterComponent;
  let fixture: ComponentFixture<RegiesterComponent>;

  beforeEach(async(() => {
    TestBedHelper.createTestBed();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegiesterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
