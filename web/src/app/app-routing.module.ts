import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import {RegiesterComponent} from './components/regiester/regiester.component'
import {ForgotpasswordComponent} from './components/forgotpassword/forgotpassword.component'
import {MaterialDashboardComponent} from './components/material-dashboard/material-dashboard.component'
const routes: Routes = [
  {

    path: '', 
    component: LoginComponent
 
 },
 {

  path: 'login',

  component: LoginComponent

},
{

  path: 'register',

  component: RegiesterComponent

},
{

  path: 'Forgotpass',

  component: ForgotpasswordComponent

},
{

  path: 'dashboard',

  component: MaterialDashboardComponent

}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
