const PROXY_CONFIG = [
  {
      context: [
          "/api",
          "/auth"
      ],
      target: "http://localhost:3010",
      secure: false
  }
]

module.exports = PROXY_CONFIG;